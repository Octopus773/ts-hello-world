import express from "express"
var app = express();

// Routes
app.get('/', function(req, res) {
  res.send('Hello World!');
});

// Routes
app.get('/hugo', function(req, res) {
  res.send('tres beau grosdoudou');
});

// Listen
var port = process.env.PORT || 3000;
console.log("Starting hello-world server");
app.listen(port, () => console.log('Listening on localhost:'+ port));
